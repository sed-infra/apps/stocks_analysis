#!/bin/sh

cd "$DJANGO_WORKDIR"
python3 "$DJANGO_WORKDIR"/manage.py collectstatic --noinput --clear

exec gunicorn -b 0.0.0.0:80 settings.wsgi:application
