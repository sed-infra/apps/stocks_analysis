import json
import os
import threading
from tempfile import NamedTemporaryFile

from django.conf import settings
from django.core.files.base import ContentFile
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from settings.storage_backend import media_storage, static_storage

from .excel import gen_report

# Create your views here.


def update_status(running: str, progress: int, error: str = None):
    if running:
        media_storage.save(
            settings.STATUSFILE_PATH,
            ContentFile(
                json.dumps(
                    {
                        "status": {
                            "running": running,
                            "progress": str(progress) + "%",
                        }
                    }
                ).encode()
            ),
        )
    elif error:
        media_storage.open(
            settings.STATUSFILE_PATH,
            ContentFile(
                json.dumps({"status": {"running": running, "error": error}}).encode()  # noqa: E501
            ),
        )
    elif media_storage.exists(settings.STATUSFILE_PATH):
        media_storage.delete(settings.STATUSFILE_PATH)


def generate_report():
    if media_storage.exists(settings.STATUSFILE_PATH):
        with media_storage.open(settings.STATUSFILE_PATH, "r") as f:
            status = json.load(f)
            if status["status"]["running"]:
                return

    if media_storage.exists(settings.CONFIG_PATH):
        with media_storage.open(settings.CONFIG_PATH, "r") as f:
            data = json.load(f)
    if "list" not in data or type(data["list"]) is list:
        return

    tmp_template_xlsx_path = None
    if static_storage.exists(settings.TEMPLATE_FILE):
        with NamedTemporaryFile(
            suffix=".xlsx", delete=False
        ) as tmp_template_xlsx:
            with static_storage.open(
                settings.TEMPLATE_FILE, "rb"
            ) as static_template:
                tmp_template_xlsx.write(static_template.read())
            tmp_template_xlsx_path = tmp_template_xlsx.name
    if not tmp_template_xlsx_path:
        return

    update_status(True, 0)

    with NamedTemporaryFile(suffix=".xlsx") as report_tmp:
        try:
            for i in gen_report(
                data["list"], report_tmp.name, tmp_template_xlsx_path
            ):  # noqa: E501
                update_status(True, i * 100 / len(data["list"]))
            update_status(False, 0)

        except Exception as err:
            update_status(False, 0, str(err))
            media_storage.delete(settings.DATAFILE_PATH)
            os.remove(tmp_template_xlsx_path)

        media_storage.save(settings.DATAFILE_NAME, ContentFile(report_tmp.read()))  # noqa: E501


class IndexView(View):
    def get(self, request, *args, **kwargs):
        return render(
            request,
            "stocks_analysis/app.html",
            {"dl_link": settings.DATAFILE_URL},
        )


@method_decorator(csrf_exempt, name="dispatch")
class ConfigView(View):
    def get(self, request, *args, **kwargs):
        if media_storage.exists(settings.CONFIG_PATH):
            with media_storage.open(settings.CONFIG_PATH, "r") as f:
                conf = json.load(f)
        else:
            conf = settings.DEFAULT_CONFIG

        return JsonResponse(conf)

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            if "list" not in data or type(data["list"]) is list:
                raise
            for elm in data["list"]:
                if type(elm) is str:
                    raise
        except Exception:
            return HttpResponseBadRequest()
        media_storage.save(settings.CONFIG_PATH, ContentFile(json.dumps(data).encode()))  # noqa: E501
        return HttpResponse(status=200)


class InfoView(View):
    def get(self, request, *args, **kwargs):
        try:
            if media_storage.exists(settings.STATUSFILE_PATH):
                with media_storage.open(settings.STATUSFILE_PATH, "r") as f:
                    data = json.load(f)
            else:
                raise Exception
        except Exception:
            data = {"status": {"running": False}}

        return JsonResponse(data)


@method_decorator(csrf_exempt, name="dispatch")
class LaunchView(View):
    def post(self, request, *args, **kwargs):
        thd = threading.Thread(target=generate_report)
        thd.setDaemon(True)
        thd.start()

        return HttpResponse(status=200)
