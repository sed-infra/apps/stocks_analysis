import os
import tempfile

from django.conf import settings
from django.test import SimpleTestCase

from .excel import add_stock, gen_report, load_template

# Create your tests here.


class RandomTest(SimpleTestCase):
    def test_success(self):
        assert True


class ExcelTest(SimpleTestCase):
    def test_feed_xlsx(self):
        if not settings.QUANDL_API_KEY:
            return  # skip

        wb = load_template()
        add_stock(wb, "EURONEXT/S30")
        add_stock(wb, "EURONEXT/AIR")

        wb.remove_sheet(wb.active)

    def test_gen_report(self):
        if not settings.QUANDL_API_KEY:
            return  # skip

        tmpf = tempfile.NamedTemporaryFile(delete=False)

        for _ in gen_report(
            ["EURONEXT/S30", "EURONEXT/AIR", "EURONEXT/ORA", "EURONEXT/EDF"],
            tmpf.name,  # noqa: E501
        ):
            pass

        assert os.path.exists(tmpf.name)

        os.unlink(tmpf.name)
