from django.urls import path

from .views import ConfigView, IndexView, InfoView, LaunchView

urlpatterns = [
    path("config", ConfigView.as_view()),
    path("info", InfoView.as_view()),
    path("launch", LaunchView.as_view()),
    path("", IndexView.as_view()),
]
