from django.apps import AppConfig


class StocksAnalysisConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "stocks_analysis"
