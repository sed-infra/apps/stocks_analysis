import os
from copy import copy

import openpyxl
import pandas
from django.conf import settings
from openpyxl.formula.translate import Translator
from openpyxl.utils import get_column_letter
from openpyxl.worksheet.worksheet import Worksheet

from .quandl import load_stock

TEMPLATE_DEFAULT_PATH = os.path.join(
    settings.STATIC_ROOT if settings.STATIC_ROOT else "",
    settings.TEMPLATE_FILE,  # noqa: E501
)


def is_formula(content):
    return content is not None and "=" in str(content)


def expand_formulas(
    sheet: Worksheet, row_src: int, row_dest: int, col_max: int = 500
) -> None:
    updated = False
    for col_id in range(1, col_max + 1):
        if is_formula(sheet.cell(row_src, col_id).value) and not is_formula(
            sheet.cell(row_dest, col_id).value
        ):
            formula = sheet.cell(row_src, col_id).value
            for row_id in range(row_src + 1, row_dest + 1):
                sheet.cell(
                    row_id,
                    col_id,
                    value=Translator(
                        formula, get_column_letter(col_id) + str(row_src)
                    ).translate_formula(
                        get_column_letter(col_id) + str(row_id)
                    ),  # noqa: E501
                )
                sheet.cell(row_id, col_id)._style = copy(
                    sheet.cell(row_src, col_id)._style
                )
            updated = True
    return updated


def load_template(filepath: str = TEMPLATE_DEFAULT_PATH) -> openpyxl.Workbook:
    return openpyxl.load_workbook(filepath)


def fill_stock_data(ws: Worksheet, data: pandas.DataFrame) -> None:
    row_id = settings.TEMPLATE_DATA_ROW_OFFSET

    for _, row in data.iterrows():
        for key, value in settings.TEMPLATE_COLUMNS_DICT.items():
            ws.cell(row_id, value, value=row[key])

            ws.cell(row_id, value)._style = copy(
                ws.cell(row_id, settings.TEMPLATE_STYLE_COL_ID)._style
            )

            if type(row[key]) is pandas._libs.tslibs.timestamps.Timestamp:
                ws.cell(row_id, value).number_format = "YYYY MMM DD"

        row_id += 1


def add_stock(wb: openpyxl.Workbook, stock: str) -> None:
    try:
        data = load_stock(stock)
    except Exception:
        raise Exception(f"Can't retrieve data for {stock}")

    ws_src = wb.active
    ws = wb.copy_worksheet(ws_src)
    ws.title = stock.replace("/", "-")

    ws[settings.TEMPLATE_SHEET_NAME_CELL] = stock

    ws.views.sheetView = ws_src.views.sheetView.copy()

    expand_formulas(
        ws,
        settings.TEMPLATE_FORMULA_EXPAND_OFFSET
        + settings.TEMPLATE_DATA_ROW_OFFSET,  # noqa: E501
        data.shape[0] + settings.TEMPLATE_DATA_ROW_OFFSET - 1,
    )

    fill_stock_data(ws, data)


def gen_report(
    stocks: list,
    filepath_out: str,
    template_filepath: str = TEMPLATE_DEFAULT_PATH,
):
    if not stocks:
        raise Exception("Empty list")
    wb = load_template(template_filepath)
    for index, elm in enumerate(stocks):
        add_stock(wb, elm)
        yield index + 1

    wb.remove_sheet(wb.active)
    wb.save(filepath_out)
