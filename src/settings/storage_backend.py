from django.conf import settings
from django.core.files.storage import FileSystemStorage
from storages.backends.s3boto3 import S3Boto3Storage


class StaticStorage(S3Boto3Storage):
    if settings.USE_S3:
        default_acl = "public-read"
        location = "static/"


class MediaStorage(S3Boto3Storage):
    if settings.USE_S3:
        default_acl = "public-read"
        location = "media/"


media_storage = (
    MediaStorage()
    if settings.USE_S3
    else FileSystemStorage(location=settings.MEDIA_ROOT)
)  # noqa: E501

static_storage = (
    StaticStorage()
    if settings.USE_S3
    else FileSystemStorage(location=settings.STATIC_ROOT)
)  # noqa: E501
