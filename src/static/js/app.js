var config_list = []
var status_running = false

function update_configlist() {
    $.post("/config", JSON.stringify({list: config_list}), function (data) {
        refresh_configlist()
    });
}

function refresh_configlist() {
    $.getJSON("/config", function (data) {
        config_list = data.list

        $("#config_list").empty()
        for (let i = 0; i < config_list.length; i++) {
            $("#config_list").append(" <li class=\"collection-item\" id=\"config_list_item_" + i.toString() + "\"><div>" + config_list[i] + "<a href=\"#!\" class=\"secondary-content\" onclick=\"delete_configlist_item(" + i.toString() + ")\"><i class=\"material-icons\">delete</i></a></div></li>")
        }
    })
}

function get_status() {
    $.getJSON("/info", function (data) {
        $("#status_text").text(data.status.running.toString())

        status_running = data.status.running

        if (status_running) {
            $("#status_running_progress").fadeIn()
            $("#status_running_progress_child").width(data.status.progress)
            $("#status_launch_button").prop("disabled", true);
            $("#dl_button").addClass("disabled");

            setTimeout(get_status, 500)
        } else {
            $("#status_running_progress").fadeOut()
            $("#status_running_progress_child").width("0%")
            $("#status_launch_button").prop("disabled", false);
            $("#dl_button").removeClass("disabled");
        }

        if (data.status.error) {
            $("#running-error").text(data.status.error)
            $("#running-error").fadeIn()
        } else {
            $("#running-error").fadeOut();
        }
    })

}

function delete_configlist_item(id) {
    config_list = config_list.filter(item => item !== config_list[id])
    update_configlist()
}

function add_configlist_item(ticket) {
    config_list.push(ticket)
    update_configlist()
}

async function launch() {
    $.post("/launch")
    await new Promise(r => setTimeout(r, 1000));
    get_status()
}

$(function () {
    refresh_configlist()
    get_status()

    $("#config_list_add_form").submit(function (event) {
        event.preventDefault();
        var ticket = $("#config_list_add_input").val()
        if (ticket !== null && ticket !== '' && !config_list.includes(ticket)) {
            add_configlist_item(ticket)
        }
        $("#config_list_add_input").val('')
    })

    $("#status_launch_form").submit(function (event) {
        event.preventDefault();
        launch()
    })
});
