FROM python:3.9-slim-bullseye

ENV DJANGO_WORKDIR="/usr/src/app"
ENV DJANGO_SETTINGS_MODULE="settings.config.production"

EXPOSE 80

RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/src/docker
WORKDIR /usr/src/app

COPY requirements.txt .

# Install python package
RUN python3 -m pip install --no-cache-dir -r requirements.txt

# Copy the application
COPY src .
COPY docker /usr/src/docker

CMD [ "/usr/src/docker/entrypoint.sh" ]
